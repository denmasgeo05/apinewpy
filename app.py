#*coding-*utf-08*-
import os, sys, tempfile, base64, requests, json, re, traceback, time, subprocess, random, shutil, atexit, ast, string, urllib, urllib.request, urllib3, html5lib, httpx, livejson
from random import randint
from io import BytesIO
from base64 import b64encode, b64decode
from hashlib import md5, sha1
from PIL import Image, ImageFilter, ImageDraw, ImageFont
from PIL.ImageFilter import (
   BLUR, CONTOUR, DETAIL, EDGE_ENHANCE, EDGE_ENHANCE_MORE,
   EMBOSS, FIND_EDGES, SMOOTH, SMOOTH_MORE, SHARPEN
)

from humanfriendly import format_timespan, format_size, format_number, format_length
#from argparse import ArgumentParser
from re import match
from urllib.parse import urlparse
from urllib.parse import quote, unquote, re
from urllib.request import urlopen
from bs4 import BeautifulSoup as bs
from bs4 import BeautifulSoup
from flask import Flask, request, send_file, render_template
try:
    import urllib.request as urllib2
except ImportError:
    import urllib2
dis_http = httpx.Client(http2=True, timeout=140)

_session = requests.Session()
_Headers = {}



app = Flask(__name__)
#==========[routess]============
@app.route('/')
def run():
    return """ <html>
                <h3>
                        STATUS : 200, OKE
                </h3>
              </html>"""
@app.route('/example/text_generator')
def home():
	return render_template('text_generator.html',title='LESSON')

@app.route('/example/media_hiburan')
def xhhome():
	return render_template('media_hiburan.html',title='LESSON')

@app.route('/example/media_18')
def this_hhome():
	return render_template('medi_18.html',title='LESSON')

if __name__ == "__main__":
	port = int(os.environ.get('PORT', 5000))
	#make_static_tmp_dir()
	app.run(host='0.0.0.0', port=port)
